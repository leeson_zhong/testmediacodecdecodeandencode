package com.octant.testmediacodec;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaFormat;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextView tv_message;
    private Button btn_decode,btn_encode;

    private Handler logHandler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            tv_message.setText(""+msg.getData().getString("message"));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_message = findViewById(R.id.tv_message);
        btn_decode = findViewById(R.id.btn_decode);
        btn_decode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FrameData.getInstance().encode = false;
                decodeThread.start();
            }
        });
        btn_encode = findViewById(R.id.btn_encode);
        btn_encode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FrameData.getInstance().encode = true;
                decodeThread.start();
                encodeThread.start();
            }
        });

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            initPermission();
        }
    }

    Thread decodeThread = new Thread(){
        @Override
        public void run() {
            super.run();
            DecodeVideo decodeVideo = new DecodeVideo();
            decodeVideo.logHandler = logHandler;
            String path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/Test.mov";
            File file = new File(path);
            if (file.exists()) {
                decodeVideo.configureMediaExtractor(path);
                decodeVideo.configureDecodeCodec();
                decodeVideo.startDecodeVideo();
                decodeVideo.close();
            }

        }
    };

    Thread encodeThread = new Thread(){
        @Override
        public void run() {
            super.run();
            int frame = 0;
            while (FrameData.getInstance().decodeStatus != 1){
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            EncodeVideo encodeVideo = new EncodeVideo();
            String path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/Encode.mov";
            //MediaFormat mediaFormat = FrameData.getInstance().getMediaFormat();
            encodeVideo.configureMediaMuxer(path);
            //encodeVideo.createOutputMediaFormat(mediaFormat.getInteger(MediaFormat.KEY_WIDTH),mediaFormat.getInteger(MediaFormat.KEY_HEIGHT),mediaFormat.getInteger(MediaFormat.KEY_FRAME_RATE),8000000);
            encodeVideo.createOutputMediaFormat();
            encodeVideo.configureEncodeCodec();
            while (true){
                try {
                    byte[] yuvData = FrameData.getInstance().pollYuvData();
                    if (yuvData != null){
                        long timeUs = FrameData.getInstance().pollPresentationTimeUsQueue();
                        encodeVideo.encodeVideo(yuvData,timeUs);
                        //encodeVideo.computePresentationTime(frame,25)
                        frame++;
                        Message msg = new Message();
                        Bundle bundle = new Bundle();
                        bundle.putString("message","encode "+frame+"frame");
                        msg.setData(bundle);
                        logHandler.sendMessage(msg);
                    }else {
                        if (FrameData.getInstance().decodeStatus == 2){
                            break;
                        }
                        Thread.sleep(10);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            encodeVideo.close();

        }
    };


    //申请多个权限
    //1、首先声明一个数组permissions，将需要的权限都放在里面
    String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
    //2、创建一个mPermissionList，逐个判断哪些权限未授予，未授予的权限存储到mPerrrmissionList中
    List<String> mPermissionList = new ArrayList<>();

    private final int mRequestCode = 200;//权限请求码


    //权限判断和申请
    private void initPermission() {

        mPermissionList.clear();//清空没有通过的权限

        //逐个判断你要的权限是否已经通过
        for (int i = 0; i < permissions.length; i++) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission( permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                    mPermissionList.add(permissions[i]);//添加还未授予的权限
                }
            }
        }

        //申请权限
        if (mPermissionList.size() > 0) {//有权限没有通过，需要申请
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions, mRequestCode);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {
            case 200:{
                initPermission();
            }break;
        }
    }
}
