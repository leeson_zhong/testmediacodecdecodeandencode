package com.octant.testmediacodec;

import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.media.Image;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * 1.configureMediaExtractor，配置MediaExtractor
 * 2.configureDecodeCodec，配置MediaCodec
 * 3.startDecodeVideo，解码视频帧
 * 4.close,结束时释放资源
 */
public class DecodeVideo {

    private MediaExtractor mediaExtractor;
    private MediaFormat mediaFormat;
    public Handler logHandler;
    private int frame;

    public void configureMediaExtractor(String videoPath){
        MediaMetadataRetriever mmr=new MediaMetadataRetriever();
        mmr.setDataSource(videoPath);
        VideoInfo videoInfo = VideoInfo.getInstance();
        videoInfo.initFromMediaMetadataRetriever(mmr);
        mmr.release();
        mediaExtractor = new MediaExtractor();
        try {
            mediaExtractor.setDataSource(videoPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < mediaExtractor.getTrackCount(); i++) {
            MediaFormat format = mediaExtractor.getTrackFormat(i);
            String mime = format.getString(MediaFormat.KEY_MIME);
            if (mime.startsWith("video/")) {
                mediaExtractor.selectTrack(i);
                mediaFormat = format;
                Log.i("==leeson", "Extractor selected track " + i + " (" + mime + "): " + format);
            }
        }
        videoInfo.initFromMediaFormat(mediaFormat);
        Log.i("==leeson", "configureMediaExtractor:"+videoInfo);
    }

    private MediaCodec mediaCodec;
    public void configureDecodeCodec(){
        String mime = mediaFormat.getString(MediaFormat.KEY_MIME);
        try {
            mediaCodec = MediaCodec.createDecoderByType(mime);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaCodec.configure(mediaFormat,null,null,0);
    }

    private static final long DEFAULT_TIMEOUT_US = 10000;
    public void startDecodeVideo(){
        mediaCodec.start();
        boolean sawInputEOS = false;
        boolean sawOutputEOS = false;
        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
        frame = 0;
        FrameData.getInstance().decodeStatus = 1;
        while (!sawOutputEOS) {
            if (!sawInputEOS) {
                int inputBufferId = mediaCodec.dequeueInputBuffer(DEFAULT_TIMEOUT_US);
                if (inputBufferId >= 0) {
                    ByteBuffer inputBuffer = mediaCodec.getInputBuffer(inputBufferId);
                    int sampleSize = mediaExtractor.readSampleData(inputBuffer, 0);
                    if (sampleSize < 0) {
                        mediaCodec.queueInputBuffer(inputBufferId, 0, 0, 0L, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                        sawInputEOS = true;
                    } else {
                        long presentationTimeUs = mediaExtractor.getSampleTime();
                        mediaCodec.queueInputBuffer(inputBufferId, 0, sampleSize, presentationTimeUs, 0);
                        mediaExtractor.advance();
                    }
                }
            }
            int outputBufferId = mediaCodec.dequeueOutputBuffer(info, DEFAULT_TIMEOUT_US);
            if (outputBufferId >= 0) {
                if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    sawOutputEOS = true;
                }
                if (info.size != 0) {
                    /**
                     * 从Android SDK 21开始，
                     * Android就开始推行新的原始（未压缩）图片数据的载体类Image，
                     * 和新的YUV格式YUV420Flexible，
                     * 配套YUV_420_888，
                     * YUV_420_888类型，其表示YUV420格式的集合，888表示Y、U、V分量中每个颜色占8bit
                     * Image保证了plane #0一定是Y，#1一定是U，#2一定是V
                     */
                    Image image = mediaCodec.getOutputImage(outputBufferId);
                    //String path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+System.currentTimeMillis()+".jpg";
                    //compressToJpeg(path,getDataFromImage(image,1),image.getCropRect().width(),image.getCropRect().height());

                    byte[] yuvData = getDataFromImage(image,1);
                    if (FrameData.getInstance().encode) {
                        while (FrameData.getInstance().offerYuvData(yuvData) == false) {
                            try {
                                Thread.sleep(10);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        FrameData.getInstance().offerPresentationTimeUsQueue(info.presentationTimeUs);
                    }else {
                        frame ++;
                        Message msg = new Message();
                        Bundle bundle = new Bundle();
                        bundle.putString("message","encode "+frame+"frame");
                        msg.setData(bundle);
                        logHandler.sendMessage(msg);
                    }
                    image.close();
//                    ByteBuffer outputBuffer = mediaCodec.getOutputBuffer(outputBufferId);
//                    MediaFormat bufferFormat = mediaCodec.getOutputFormat(outputBufferId);
//                    frame ++;
//                    Log.i("==leeson", "startDecodeVideo,frame=" + frame + ",length=" + (outputBuffer.limit()-outputBuffer.position()) + ",color=" + bufferFormat.getInteger(MediaFormat.KEY_COLOR_FORMAT));
//                    /**
//                     * YUV格式通常有两大类：打包（packed）格式和平面（planar）格式。
//                     * 对于planar的YUV格式，先连续存储所有像素点的Y，紧接着存储所有像素点的U，随后是所有像素点的V。
//                     * 对于packed的YUV格式，每个像素点的Y,U,V是连续交叉存储的。
//                     * YUV420： 即打包格式的YUV420
//                     * YUV420P: 即YUV420 Planar，Y\U\V数据是分开存放的
//                     * YUV420SP:即YUV420 semi planar, 这个格式的数据量跟YUV420 Planar的一样，但是U、V是交叉存放的
//                     * OMX_COLOR_FormatYUV420SemiPlanar = PIX_FMT_NV12 YYYY UVUV
//                     * OMX_COLOR_FormatYUV420Planar = PIX_FMT_YUV420P YYYY UU VV
//                     */
//                    switch (bufferFormat.getInteger(MediaFormat.KEY_COLOR_FORMAT)) {
//
//                        case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV411Planar:
//                            break;
//                        case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV411PackedPlanar:
//                            break;
//                        case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedPlanar:
//                            break;
//                        case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar:
//                            break;
//                        case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedSemiPlanar:
//                            break;
//                        case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar:
//                        default:
//                            break;
//                    }
//
                }
                mediaCodec.releaseOutputBuffer(outputBufferId, false);
            }
        }
        FrameData.getInstance().decodeStatus = 2;
    }

    public void close() {
        try {
            mediaExtractor.release();
            mediaCodec.stop();
            mediaCodec.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //标准的YUV42OP也就是NV21
    public static void compressToJpeg(String fileName, byte[] yuvData,int width,int height) {
        FileOutputStream outStream;
        try {
            outStream = new FileOutputStream(fileName);
        } catch (IOException ioe) {
            throw new RuntimeException("Unable to create output file " + fileName, ioe);
        }
        YuvImage yuvImage = new YuvImage(yuvData, ImageFormat.NV21, width, height, null);
        yuvImage.compressToJpeg(new Rect(0,0,width,height), 100, outStream);
    }

    public static byte[] getDataFromImage(Image image, int colorFormat) {
        boolean VERBOSE = false;
        int COLOR_FormatI420 = 0,COLOR_FormatNV21=1;
        String TAG = "getDataFromImage";

        if (colorFormat != COLOR_FormatI420 && colorFormat != COLOR_FormatNV21) {
            throw new IllegalArgumentException("only support COLOR_FormatI420 " + "and COLOR_FormatNV21");
        }
        if (!isImageFormatSupported(image)) {
            throw new RuntimeException("can't convert Image to byte array, format " + image.getFormat());
        }
        Rect crop = image.getCropRect();
        int format = image.getFormat();
        int width = crop.width();
        int height = crop.height();
        Image.Plane[] planes = image.getPlanes();
        byte[] data = new byte[width * height * ImageFormat.getBitsPerPixel(format) / 8];
        byte[] rowData = new byte[planes[0].getRowStride()];
        if (VERBOSE) Log.v(TAG, "get data from " + planes.length + " planes");
        int channelOffset = 0;
        int outputStride = 1;
        for (int i = 0; i < planes.length; i++) {
            switch (i) {
                case 0:
                    channelOffset = 0;
                    outputStride = 1;
                    break;
                case 1:
                    if (colorFormat == COLOR_FormatI420) {
                        channelOffset = width * height;
                        outputStride = 1;
                    } else if (colorFormat == COLOR_FormatNV21) {
                        channelOffset = width * height + 1;
                        outputStride = 2;
                    }
                    break;
                case 2:
                    if (colorFormat == COLOR_FormatI420) {
                        channelOffset = (int) (width * height * 1.25);
                        outputStride = 1;
                    } else if (colorFormat == COLOR_FormatNV21) {
                        channelOffset = width * height;
                        outputStride = 2;
                    }
                    break;
            }
            ByteBuffer buffer = planes[i].getBuffer();
            int rowStride = planes[i].getRowStride();
            int pixelStride = planes[i].getPixelStride();
            if (VERBOSE) {
                Log.v(TAG, "pixelStride " + pixelStride);
                Log.v(TAG, "rowStride " + rowStride);
                Log.v(TAG, "width " + width);
                Log.v(TAG, "height " + height);
                Log.v(TAG, "buffer size " + buffer.remaining());
            }
            int shift = (i == 0) ? 0 : 1;
            int w = width >> shift;
            int h = height >> shift;
            buffer.position(rowStride * (crop.top >> shift) + pixelStride * (crop.left >> shift));
            for (int row = 0; row < h; row++) {
                int length;
                if (pixelStride == 1 && outputStride == 1) {
                    length = w;
                    buffer.get(data, channelOffset, length);
                    channelOffset += length;
                } else {
                    length = (w - 1) * pixelStride + 1;
                    buffer.get(rowData, 0, length);
                    for (int col = 0; col < w; col++) {
                        data[channelOffset] = rowData[col * pixelStride];
                        channelOffset += outputStride;
                    }
                }
                if (row < h - 1) {
                    buffer.position(buffer.position() + rowStride - length);
                }
            }
            if (VERBOSE) Log.v(TAG, "Finished reading data from plane " + i);
        }
        return data;
    }

    private static boolean isImageFormatSupported(Image image) {
        int format = image.getFormat();
        switch (format) {
            case ImageFormat.YUV_420_888:
            case ImageFormat.NV21:
            case ImageFormat.YV12:
                return true;
        }
        return false;
    }

    private byte[] getYUVI420DataFromImage(Image image){
        Image.Plane[] planes = image.getPlanes();
        int format;
        if (image.getFormat() == ImageFormat.YUV_420_888){
            Rect crop = image.getCropRect();//cropRect获取的width和height才是准确的
            int width = crop.width();
            int height = crop.height();
            byte[] yuvData = new byte[width * height * ImageFormat.getBitsPerPixel(image.getFormat()) / 8];
            byte[] rowData = new byte[planes[0].getRowStride()];
            int dataOffset = 0;
            int rowStride;//每行间隔，每行有效像素从rowStride*row开始
            int pixelStride;//一行每个像素间隔，后一个有效像素是前一个有效像素坐标加上pixelStride。比如pixelStride=2，有效像素是0、2、4、8
            int rowLen;//行数
            int colLen;//列数
            for (int i=0; i<planes.length; i++){
                rowStride = planes[i].getRowStride();
                pixelStride = planes[i].getPixelStride();
                ByteBuffer buffer = planes[i].getBuffer();
                if (i == 0){
                    //Y
                    rowLen = height;
                    colLen = width;
                }else {
                    //U或V，4个Y配1个U和1个V,行和列都少一半
                    rowLen = height >> 1;
                    colLen = width >> 1;
                }
                for (int row=0; row<rowLen; row++){
                    if (pixelStride == 1){
                        //直接复制每行数据
                        buffer.position(rowStride*row);
                        buffer.get(yuvData,dataOffset,colLen);
                        dataOffset += colLen;
                    }else {
                        //每行数据，要间隔复制
                        buffer.position(rowStride*row);
                        buffer.get(rowData,0,colLen*pixelStride);
                        for (int col = 0;col<colLen; col++){
                            yuvData[dataOffset] = rowData[col*pixelStride];
                            dataOffset++;
                        }
                    }
                }
            }
            return yuvData;
        }
        return null;
    }
}
